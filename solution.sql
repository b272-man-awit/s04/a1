-- A)
SELECT * FROM artists WHERE name LIKE "%d%";

-- B)
SELECT * FROM songs WHERE length < 230;

-- C)
SELECT songs.song_name, songs.length, albums.album_title FROM songs JOIN albums ON albums.id = songs.album_id;

-- D)
SELECT * FROM albums JOIN artists ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- E)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 0, 4;

-- F)
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;